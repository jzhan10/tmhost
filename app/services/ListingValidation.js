/**
 * Listing Validation Service
 */
app.factory('ListingValidation', function() {
    return {
	    isUrl: function(str) {
	        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
	            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
	            '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
	            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
	            '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
	            '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
	        if(!pattern.test(str)) {
	            return false;
	        } else {
	            return true;
	        }
	    },
		getListingTypeEnum(type) {
	        if (type.business && type.leisure) {
	            return 'both';
	        } else if (type.business){
	            return 'business';
	        } else {
	            return 'leisure';
	        }
	    },
		validateCreateRequired: function(firstName, lastName, email, type, description) {
	        if (firstName === '') {
	            return 'Please enter a first name.';
	        } else if (lastName === '') {
	            return 'Please enter a last name.';
	        } else if (email === '') {
	            return 'Please enter an email.';
	        } else if (!type.business && !type.leisure) {
	            return 'Please check business, leisure, or both.';
	        } else if (description === '') {
	            return 'Please enter a description.';
	        } else {
	            return '';
	        }
		},
        validateEditRequired: function(name, type, description) {
	        if (name === '') {
	            return 'Please enter a name.';
	        } else if (!type.business && !type.leisure) {
	            return 'Please check business, leisure, or both.';
	        } else if (description === '') {
	            return 'Please enter a description.';
	        } else {
	            return '';
	        }
        },
		validateImageUrls: function(images) {
			for (var i in images) {
				if (images[i] && images[i].length > 0 && !this.isUrl(images[i])) {
					return 'Image URL is not valid, please re-check.'
				}
			}
			return '';
		},
		validateOther: function(email, description, images) {
	        if (email && email.indexOf('@') > -1) {
	            return 'Email field does not need @tubemogul.com.';
	        } else if (description.length <= 5) {
	            return 'Please enter a description longer than 5 characters'
	        } else {
	            return this.validateImageUrls(images);
	        }
		},
	    addImageFieldsToData: function(data, images) {
	        var imagesArr = [];
	        var imagesJson = {};
            data['image_link'] = '';
			for (var i in images) {
		        if (images[i] && images[i] !== '') {
		            imagesArr.push(images[i]);
		        }
			}
	        if (imagesArr.length > 0) {
	            for (var i in imagesArr) {
	                imagesJson['image_' + i] = imagesArr[i];
	            }
	            data['image_link'] = JSON.stringify(imagesJson);
	        }
	        return data;
	    },
	    prepareForUpdate: function(data) {
	        // Separate data from $scope.listing pointer
	        var d = {};
	        var fields = [
	            'name',
	            'email',
	            'auth_code',
	            'location_id',
	            'type',
	            'image_link',
	            'description'
	        ]
	        for (var i in data) {
	            if (fields.indexOf(i) > -1) {
	                d[i] = data[i];
	            }
	        }
	        d.type = this.getListingTypeEnum(d.type);
	        return d;
	    }
	}
});
