/**
 * Listing Service
 *
 * @param QueryService
 * @param Location
 */
app.factory('Listing', function(QueryService, Location) {
    var defaultImg = '/images/locations/default-bkgd.png';
    return {
        getLoadingListing: function() {
            return {
                name: '...',
                location: '...',
                description: '...',
                type: '...'
            };
        },
        getDefaultListing: function() {
            return {
                name: 'N/A',
                location: 'N/A',
                description: 'N/A',
                type: 'N/A'
            };
        },
        getListingForUser: function(email) {
            return QueryService.query('GET', host + '/v1/tubebnb/listings/get/' + email, {}, {});
        },
        getListings: function() {
            return QueryService.query('GET', host + '/v1/tubebnb/listings/get_all/', {}, {});
        },
        getListing: function(listings, id) {
            listings = listings.data;
            for (var i in listings) {
                if (listings[i].listing_id == id) {
                    return listings[i];
                }
            }
            return {};
        },
        createListing: function(data) {
            return QueryService.query('POST', host + '/v1/tubebnb/listings/create/', {}, formatData(data));
        },
        updateListing: function(id, data) {
            return QueryService.query('POST', host + '/v1/tubebnb/listings/update/' + id, {}, formatData(data));
        },
        deleteListing: function(data) {
            return QueryService.query('POST', host + '/v1/tubebnb/listings/delete/', {}, formatData(data));
        },
        forceDelete: function(id) {
            this.deleteListing({listing_id: id});
        },
        convertBadImageLink: function(link) {
            var badImgurLink = 'http://imgur.com/';
            var imgurPos = link.indexOf(badImgurLink);
            if (imgurPos > -1) {
                return 'http://i.imgur.com/' + link.split(badImgurLink)[1] + '.jpg';
            }
            return link;
        },
        convertImageLinkToArray: function(listing) {
            if (listing.image_link) {
                if (listing.image_link === defaultImg) {
                    return [defaultImg];
                } else {
                    var linksArr = [];
                    var links = JSON.parse(listing.image_link);
                    for (var i in links) {
                        link = this.convertBadImageLink(links[i]);
                        linksArr.push(link);
                    }
                    return linksArr;
                }
            } else {
                return [];
            }
        },
        modifyValues: function(locations, listing) {
            var t = listing.type;
            var t_raw = {'business': false, 'leisure': false};
            if (t === 'business') {
                t = 'Business';
                t_raw.business = true;
            } else if (t === 'leisure') {
                t = 'Leisure';
                t_raw.leisure = true;
            } else {
                t = 'Business, Leisure';
                t_raw.business = true;
                t_raw.leisure = true;
            }
            listing.name = listing.name || 'N/A';
            listing.type = t;
            listing.temp_type = t;
            listing.type_raw = t_raw;
            listing.location = _.pluck(_.filter(locations, {'location_id': listing.location_id }), 'name')[0];
            var location = Location.getLocation(locations, listing.location);

            var images = this.convertImageLinkToArray(listing);
            if (images.length > 0) {
                listing.image_link = images[0];
                listing.images = images;
                listing.defaultImage = '';
                listing.useDefaultImage = false;
            } else {
                listing.images = [];
                listing.image_link = listing.defaultImage = location.image_link;
                listing.useDefaultImage = true;
            }
            listing.imageUrl = images.length > 0 ? images[0] : null;
            listing.imageUrl2 = images.length > 1 ? images[1] : null;
            listing.imageUrl3 = images.length > 2 ? images[2] : null;
            return listing;
        },
        hasImage: function(listing) {
            return listing.image_link !== '' && listing.image_link !== defaultImg;
        },
        modifyImageLink: function(listing, locations) {
            if (!this.hasImage(listing)) {
                var location = Location.getLocation(locations, listing.location);
                listing.image_link = location.image_link;
                return true;
            } else {
                return false;
            }
        },
        sendAuthCode: function(data) {
            return QueryService.query('POST', host + '/v1/tubebnb/email/send_auth/', {}, formatData(data));
        },
        generateAuthCode: function(len) {
            var text = ''
            var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

            for (var i = 0; i < len; i++ ) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text;
        }
    }
});
