/**
 * Location Service
 *
 * @param QueryService
 */
app.factory('Location', function(QueryService) {
    // Private functions
    var getLocationImage = function(name) {
        return '/images/locations/' + name.replace(/\s/g, '').toLowerCase() + '.png';
    };

    return {
        getLocation: function(locations, name) {
            return _.find(locations, {name: name});
        },
        getLocations: function() {
            return QueryService.query('GET', host + '/v1/tubebnb/locations/get_all/', {}, {});
        },
        isValidLocation: function(name) {
            return name && name !== '...' && name !== '';
        },
        modifyLocations: function(locations) {
            for (var i in locations) {
                locations[i].image_link = getLocationImage(locations[i].name);
            }
            return locations;
        },
        getInfoPage: function(location) {
            switch(location) {
                case 'Emeryville':
                    return 'https://en.wikipedia.org/wiki/Emeryville,_California';
                    break;
                case 'Chicago':
                    return 'https://en.wikipedia.org/wiki/Chicago';
                    break;
                case 'Detroit':
                    return 'https://en.wikipedia.org/wiki/Detroit';
                    break;
                case 'Kyiv':
                    return 'https://en.wikipedia.org/wiki/Kiev';
                    break;
                case 'London':
                    return 'https://en.wikipedia.org/wiki/London';
                    break;
                case 'LA':
                    return 'https://en.wikipedia.org/wiki/Los_Angeles';
                    break;
                case 'Minneapolis':
                    return 'https://en.wikipedia.org/wiki/Minneapolis';
                    break;
                case 'New York':
                    return 'https://en.wikipedia.org/wiki/New_York_City';
                    break;
                case 'Sao Paulo':
                    return 'https://en.wikipedia.org/wiki/São_Paulo';
                    break;
                case 'Singapore':
                    return 'https://en.wikipedia.org/wiki/Singapore';
                    break;
                case 'Shanghai':
                    return 'https://en.wikipedia.org/wiki/Shanghai';
                    break;
                case 'Sydney':
                    return 'https://en.wikipedia.org/wiki/Sydney';
                    break;
                case 'Tokyo':
                    return 'https://en.wikipedia.org/wiki/Tokyo';
                    break;
                case 'Toronto':
                    return 'https://en.wikipedia.org/wiki/Toronto';
                    break;
                case 'Chengdu':
                    return 'https://en.wikipedia.org/wiki/Chengdu';
                    break;
                case 'Other':
                    return 'https://www.tubemogul.com';
                    break;
            }
        }
    }
});
