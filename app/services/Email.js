/**
 * Email Service
 * In format:   listing_id
 *              seeker_email
 *              description
 *
 * @param QueryService
 */
app.factory('Email', function(QueryService) {
    return {
        createEmail: function(data) {
            return QueryService.query('POST', host + '/v1/tubebnb/email/create/', {}, formatData(data));
        }
    }
});
