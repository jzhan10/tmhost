/**
 * Main application controller
 *
 * You can use this controller for your whole app if it is small
 * or you can have separate controllers for each logical section
 */
;(function() {
    angular
    .module('boilerplate')
        .controller('MainController', MainController)
        .controller('HeaderController', HeaderController)
        .controller('HostApplyController', HostApplyController)
        .controller('SeekHostController', SeekHostController)
        .controller('HostListingController', HostListingController)
        .controller('HostDetailController', HostDetailController)
        .controller('SeekListingController', SeekListingController)
        .controller('SeekDetailController', SeekDetailController);

    // Inject services
    SeekHostController.$inject = ['LocalStorage', 'QueryService', '$scope', 'Location'];
    SeekDetailController.$inject = ['LocalStorage', 'QueryService', '$scope', '$routeParams',
        'Location', 'Listing', 'Email'];
    SeekListingController.$inject = ['LocalStorage', 'QueryService', '$scope', '$location',
        'Location', 'Listing'];
    HostListingController.$inject = ['LocalStorage', 'QueryService', '$scope', '$location', 'Location', 'Listing'];
    HostDetailController.$inject = ['LocalStorage', 'QueryService', '$scope', '$routeParams',
        'Location', 'Listing'];
    HostApplyController.$inject = ['LocalStorage', 'QueryService', '$scope', '$location',
        '$window', 'Location', 'Listing'];
    MainController.$inject = ['LocalStorage', 'QueryService', '$scope', 'Location', '$location'];
    HeaderController.$inject = ['$scope'];

    function MainController(LocalStorage, QueryService, $scope, Location, $location) {
        // 'controller as' syntax
        var self = this;
        $scope.locations = Location.getLocations(QueryService);
        $scope.goToTravel = function ( ) {
            $location.path( "/seek" );
        };

        $scope.goToHost = function ( ) {
            $location.path( "/host/create" );
            window.scrollTo(0,0);
        };
    }

    function HeaderController($scope) {
        function setSelected() {
            var path = location.hash;
            switch (path) {
                case '#/host/create':
                    $scope.menuSelected = 'host_create';
                    break;
                case '#/host':
                    $scope.menuSelected = 'host';
                    break;
                case '#/seek':
                    $scope.menuSelected = 'seek';
                    break;
                default:
                    $scope.menuSelected = '';
            }
        }
        setSelected();
        $scope.$on('$routeChangeSuccess', function(scope, next, current){
            setSelected();
        });

    }

    /**
     * Host Apply controller
     * Endpoint: /host/create
     *
     * @param LocalStorage
     * @param QueryService
     * @param $scope
     * @param $location
     * @param $window
     * @param Location
     * @param Listing
     */
    function HostApplyController(
        LocalStorage,
        QueryService,
        $scope,
        $location,
        $window,
        Location,
        Listing
    )
    {
        var self = this;
        Location.getLocations(QueryService).then(function(data) {
            $scope.locations = data.data;
            $scope.location = $scope.locations[0]['location_id'];
        });
        $scope.isError = false;
        $scope.errorMsg = '';

        // Init Form Vars
        $scope.item = 'apply_host';
        $scope.firstName = '';
        $scope.lastName = '';
        $scope.email = '';
        $scope.description = '';
        $scope.type = { business: false, leisure: false };

        // Validation
        function validateRequired() {
            if ($scope.firstName === '') {
                return 'Please enter a first name.';
            } else if ($scope.lastName === '') {
                return 'Please enter a last name.';
            } else if ($scope.email === '') {
                return 'Please enter an email.';
            } else if (!$scope.type.business && !$scope.type.leisure) {
                return 'Please check business, leisure, or both.';
            } else if ($scope.description === '') {
                return 'Please enter a description.';
            } else {
                return '';
            }
        }

        function validateOther() {
            if ($scope.description.length <= 5) {
                return 'Please enter a description longer than 5 characters'
            } else {
                return '';
            }
        }

        function hasErrorMsg(msg) {
            return msg.length > 0;
        }

        function setErrorMsg(msg) {
            $scope.isError = true;
            $scope.errorMsg = msg;

            $window.scrollTo(0, 0);
        }

        function getTypeEnum() {
            if ($scope.type.business && $scope.type.leisure) {
                return 'both';
            } else if ($scope.type.business){
                return 'business';
            } else {
                return 'leisure';
            }
        }

        $scope.validate = function() {
            // Validate Required
            errorMsg = validateRequired();
            if (hasErrorMsg(errorMsg)) {
                setErrorMsg(errorMsg);
                return false;
            }

            // Validate Required
            errorMsg = validateOther();
            if (hasErrorMsg(errorMsg)) {
                setErrorMsg(errorMsg);
                return false;
            }

            // Create Listing
            var data = {
                name: $scope.firstName + ' ' + $scope.lastName,
                email: $scope.email + '@tubemogul.com',
                location_id: $scope.location,
                type: getTypeEnum(),
                description: $scope.description
            };
            Listing.createListing(data).then(function() {
                // Success
                $scope.isError = false;
                $scope.errorMsg = '';
                $location.path('/host');
            }, function() {
                // Error
                $scope.isError = true;
                $scope.errorMsg = 'There was an error while saving. Please try again.';
            });
        };
    }

    /**
     * Host Listing controller
     * Endpoint: /host
     *
     * @param LocalStorage
     * @param QueryService
     * @param $scope
     * @param $location
     * @param Location
     * @param Listing
     */
    function HostListingController(
        LocalStorage,
        QueryService,
        $scope,
        $location,
        Location,
        Listing
    )
    {
        var self = this;
        var locations, locationName;
        $scope.title = 'N/A';
        $scope.showNoGrid = true;
        $scope.gettingListing = false;
        $scope.gettingListingMsg = '';
        Location.getLocations(QueryService).then(function(l) {
            locations = l.data;
        });

        function getListingForUser(email) {
            $scope.title = '';
            Listing.getListingForUser(email).then(function(data) {
                if ('data' in data && typeof data.data.name !== 'undefined') {
                    data = data.data;
                    var name = data.name || 'N/A';
                    $scope.title = name + ' - ' + data.email;
                    $scope.listing = Listing.modifyValues(locations, data);
                    $scope.showNoGrid = false;
                } else {
                    $scope.title = 'N/A';
                    $scope.listing = false;
                    $scope.showNoGrid = true;
                }
                $scope.gettingListing = false;
                $scope.gettingListingMsg = '';
            }, function() {
                $scope.listing = false;
                $scope.title = 'N/A';
                $scope.showNoGrid = true;
                $scope.gettingListing = false;
                $scope.gettingListingMsg = '';
            });
        }

        $scope.getListing = function() {
            $scope.gettingListing = true;
            $scope.gettingListingMsg = 'Fetching listing...';
            $scope.listing = false;
            getListingForUser($scope.email + '@tubemogul.com');
        };

        $scope.goToHostDetail = function(hostId) {
            $location.path('/host/' + hostId);
        };
    }

    /**
     * Seek Listing controller
     * Endpoint: /seek
     *
     * @param LocalStorage
     * @param QueryService
     * @param $scope
     * @param $location
     * @param Location
     * @param Listing
     */
    function SeekListingController(
        LocalStorage,
        QueryService,
        $scope,
        $location,
        Location,
        Listing
    )
    {
        var self = this;
        var locations, locationName;
        $scope.locationIds = [1];

        Location.getLocations(QueryService).then(function(l) {
            $scope.locations = l.data;

            Listing.getListings().then(function(data) {
                $scope.listings = data.data;
                for (i in $scope.listings) {
                    $scope.listings[i] = Listing.modifyValues($scope.locations, $scope.listings[i]);
                }
            }, function() {
                $scope.listings = [];
            });
        });
        $scope.filterClick = function(locationId) {
            var index = $scope.locationIds.indexOf(locationId);
            if (index > -1) {
                $scope.locationIds.splice(index, 1);
            } else {
                $scope.locationIds.push(locationId);
            }
        };
        $scope.filter = function(listing) {
            return $scope.locationIds.indexOf(listing.location_id) > -1;
        };

        $scope.goToSeekDetail = function(seekId) {
            $location.path('/seek/' + seekId);
        }
    }

    /**
     * Host Detail controller
     * Endpoint: /host/:id
     *
     * @param LocalStorage
     * @param QueryService
     * @param $scope
     * @param $routeParams
     * @param Location
     * @param Listing
     */
    function HostDetailController(
        LocalStorage,
        QueryService,
        $scope,
        $routeParams,
        Location,
        Listing
    )
    {
        var self = this;
        var listingId = $routeParams.id;
        var locations, locationName;
        $scope.showAuthCode = false;

        Location.getLocations(QueryService).then(function(l) {
            locations = l.data;

            Listing.getListings().then(function(data) {
                var listing = Listing.getListing(data, listingId);
                if ('name' in listing) {
                    $scope.listing = Listing.modifyValues(locations, listing);
                } else {
                    $scope.listing = {};
                }
            }, function() {
                $scope.listing = {};
                $scope.title = 'N/A';
            });
        });

        $scope.showAuthCodeBox = function() {
            $scope.showAuthCode = true;
        };
    }

    /**
     * Seek Host controller (deprecated)
     * Endpoint: /host/seek
     *
     * @param LocalStorage
     * @param QueryService
     * @param $scope
     * @param Location
     */
    function SeekHostController(LocalStorage, QueryService, $scope, Location) {
       var self = this;
       $scope.locations = Location.getLocations(QueryService);
    }

    /**
     * Seek Detail controller
     * Endpoint: /seek/:id
     *
     * @param LocalStorage
     * @param QueryService
     * @param $scope
     * @param $routeParams
     * @param Location
     * @param Listing
     * @param Email
     */
    function SeekDetailController(
        LocalStorage,
        QueryService,
        $scope,
        $routeParams,
        Location,
        Listing,
        Email
    )
    {
        var self = this;
        var listingId = $routeParams.id;
        var locations, locationName;
        $scope.email = '';
        $scope.isReserving = false;
        $scope.reserveText = 'Reserve';

        Location.getLocations(QueryService).then(function(l) {
            locations = l.data;

            Listing.getListings().then(function(data) {
                var listing = Listing.getListing(data, listingId);
                if ('name' in listing) {
                    $scope.listing = Listing.modifyValues(locations, listing);
                } else {
                    $scope.listing = {};
                }
            }, function() {
                $scope.listing = {};
                $scope.title = 'N/A';
            });
        });

        $scope.reserve = function() {
            $scope.isSuccess = false;
            $scope.successMsg = '';
            if ($scope.email.length > 0 && !$scope.isReserving) {
                $scope.isSuccess = false;
                $scope.successMsg = '';
                $scope.isReserving = true;
                $scope.reserveText = 'Reserving...';

                var data = {
                    listing_id: listingId,
                    seeker_email: $scope.email + '@tubemogul.com',
                    description: $scope.description
                };
                Email.createEmail(data).then(function() {
                    $scope.email = '';
                    $scope.description = '';
                    $scope.isSuccess = true;
                    $scope.successMsg = 'Successfully made a request!';
                    $scope.isReserving = false;
                    $scope.reserveText = 'Reserve';
                });
            }
        };
    }
})();
