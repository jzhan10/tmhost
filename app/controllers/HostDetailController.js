/**
 * Host Detail controller
 * Endpoint: /host/:id
 *
 * @param LocalStorage
 * @param QueryService
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param $window
 * @param Location
 * @param Listing
 * @param ListingValidation
 */
app.controller('HostDetailController', function(
    LocalStorage,
    QueryService,
    $scope,
    $routeParams,
    $location,
    $window,
    Location,
    Listing,
    ListingValidation
)
{
    var self = this;
    var listingId = $routeParams.id;
    var locations, locationName;
    $scope.showAuthCode =
        $scope.editMode =
        $scope.editSuccess =
        $scope.editError =
        $scope.isError =
        $scope.isSuccess = false;

    $scope.editSuccessMsg =
        $scope.editErrorMsg =
        $scope.errorMsg =
        $scope.successMsg = '';
    $scope.listing = Listing.getLoadingListing();
    $scope.locationInfo = '';
    $scope.useDefaultLocation = false;

    // Init
    Location.getLocations(QueryService).then(function(l) {
        $scope.locations = Location.modifyLocations(l.data);

        Listing.getListings().then(function(data) {
            var listing = Listing.getListing(data, listingId);
            if ('name' in listing) {
                $scope.listing = Listing.modifyValues($scope.locations, listing);
                $scope.useDefaultLocation = Listing.modifyImageLink($scope.listing, $scope.locations);
                var locIndex = _.findIndex($scope.locations, { 'name': $scope.listing.location });
                $scope.location = $scope.locations[locIndex]['location_id'];
                $scope.locationInfo = Location.getInfoPage($scope.listing.location);
            } else {
                $scope.listing = Listing.getDefaultListing();
                $scope.location = $scope.locations[0]['location_id'];
            }
        }, function() {
            $scope.listing = Listing.getDefaultListing();
            $scope.title = 'N/A';
            $scope.location = $scope.locations[0]['location_id'];
        });
    });

    $scope.goToHostListings = function() {
        $location.path('/host');
    };

    $scope.showAuthCodeBox = function() {
        $scope.showAuthCode = true;
    };

    var setAuthError = function(errMsg) {
        $scope.errorMsg = errMsg;
        $scope.isError = true;
        $scope.successMsg = '';
        $scope.isSuccess = false;
    };

    var setAuthSuccess = function(successMsg) {
        $scope.errorMsg = '';
        $scope.isError = false;
        $scope.successMsg = successMsg;
        $scope.isSuccess = true;
    };

    var clearAuthMessage = function() {
        $scope.errorMsg = '';
        $scope.isError = false;
        $scope.successMsg = '';
        $scope.isSuccess = false;
    };

    $scope.toggleEdit = function() {
        if ($scope.authCode !== $scope.listing.auth_code) {
            setAuthError('Invalid auth code!');
            $scope.editMode = false;
            $scope.listing.type = $scope.listing.temp_type;
        } else {
            setAuthSuccess('Success! Editing Listing...');
            $scope.editMode = true;
            $scope.listing.temp_type = $scope.listing.type;
            $scope.listing.type = $scope.listing.type_raw;
        }
    };

    $scope.delete = function() {
        if ($scope.editMode && confirm('Are you sure you want to delete this listing?')) {
            var data = {listing_id: listingId};
            Listing.deleteListing(data).then(function(data) {
                $location.path('/host');
                $window.scrollTo(0, 0);
            });
        }
    };

    function hasErrorMsg(msg) {
        return msg.length > 0;
    }

    function setErrorMsg(msg) {
        $scope.editError = true;
        $scope.editErrorMsg = msg;
        $window.scrollTo(0, 0);
    }

    $scope.update = function() {
        $scope.editSuccess =
            $scope.editError = false;
        $scope.editSuccessMsg =
            $scope.editErrorMsg = '';
        if ($scope.editMode) {
            errorMsg = ListingValidation.validateEditRequired(
                $scope.listing.name,
                $scope.listing.type,
                $scope.listing.description
            );
            if (hasErrorMsg(errorMsg)) {
                setErrorMsg(errorMsg);
                return false;
            }

            // Validate Required
            errorMsg = ListingValidation.validateOther(
                false,
                $scope.listing.description,
                [
                    $scope.listing.imageUrl,
                    $scope.listing.imageUrl2,
                    $scope.listing.imageUrl3
                ]
            );
            if (hasErrorMsg(errorMsg)) {
                setErrorMsg(errorMsg);
                return false;
            }
            var data = ListingValidation.prepareForUpdate($scope.listing);
            data = ListingValidation.addImageFieldsToData(
                data,
                [
                    $scope.listing.imageUrl,
                    $scope.listing.imageUrl2,
                    $scope.listing.imageUrl3
                ]
            );
            Listing.updateListing(listingId, data).then(function(data) {
                // Success!
                $scope.editSuccess = true;
                $scope.editSuccessMsg = 'Successfully edited your listing! ' +
                    'Refresh this page to go back to viewer mode!';
                $window.scrollTo(0, 0);
            }, function() {
                $scope.editError = false;
                $scope.editErrorMsg = 'There was a problem while saving. Please try again later.';
            });
        }
    };

    $scope.resendAuthCode = function() {
        if (confirm('Resend auth code to ' + $scope.listing.email + '?')) {
            // Send Auth Code
            Listing.sendAuthCode({email: $scope.listing.email}).then(function(data) {
                setAuthSuccess('Success! Resent auth code to ' + $scope.listing.email + '.');
            }, function() {
                setAuthError('There was a problem while sending the auth code.');
            });
        }
    };
});
