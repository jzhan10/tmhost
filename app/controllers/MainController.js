/**
 * Main Controller
 *
 * @param LocalStorage
 * @param QueryService
 * @param $scope
 * @param Location
 * @param $location
 */
app.controller('MainController', function(
	LocalStorage,
	QueryService,
	$scope,
	Location,
	$location
)
{
    // 'controller as' syntax
    var self = this;
    $scope.locations = Location.getLocations(QueryService);
    $scope.goToTravel = function ( ) {
        $location.path( "/seek" );
    };

    $scope.goToHost = function ( ) {
        $location.path( "/host/create" );
        window.scrollTo(0,0);
    };
});
