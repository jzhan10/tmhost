/**
* Host Apply controller
* Endpoint: /host/create
*
* @param LocalStorage
* @param QueryService
* @param $scope
* @param $location
* @param $window
* @param Location
* @param Listing
* @param ListingValidation
*/
app.controller('HostApplyController', function(
    LocalStorage,
    QueryService,
    $scope,
    $location,
    $window,
    Location,
    Listing,
    ListingValidation
)
{
    var self = this;
    Location.getLocations(QueryService).then(function(data) {
        $scope.locations = data.data;
        $scope.location = $scope.locations[0]['location_id'];
    });
    // Init Form Vars
    $scope.isError = false;
    $scope.item = 'apply_host';
    $scope.firstName =
        $scope.lastName =
        $scope.email =
        $scope.imageUrl =
        $scope.imageUrl2 =
        $scope.imageUrl3 =
        $scope.description =
        $scope.errorMsg = '';
    $scope.type = { business: false, leisure: false };

    function hasErrorMsg(msg) {
        return msg.length > 0;
    }

    function setErrorMsg(msg) {
        $scope.isError = true;
        $scope.errorMsg = msg;
        $window.scrollTo(0, 0);
    }

    /**
     * Validation Function - Need to break up
     */
    $scope.validate = function() {
        // Validate Required
        errorMsg = ListingValidation.validateCreateRequired(
            $scope.firstName,
            $scope.lastName,
            $scope.email,
            $scope.type,
            $scope.description
        );
        if (hasErrorMsg(errorMsg)) {
            setErrorMsg(errorMsg);
            return false;
        }

        // Validate Other
        errorMsg = ListingValidation.validateOther(
            $scope.email,
            $scope.description,
            [
                $scope.imageUrl,
                $scope.imageUrl2,
                $scope.imageUrl3
            ]
        );
        if (hasErrorMsg(errorMsg)) {
            setErrorMsg(errorMsg);
            return false;
        }

        // Prepare Create Listing
        var data = {
            name: $scope.firstName + ' ' + $scope.lastName,
            email: $scope.email + '@tubemogul.com',
            location_id: $scope.location,
            type: ListingValidation.getListingTypeEnum($scope.type),
            description: $scope.description,
            auth_code: Listing.generateAuthCode(6)
        };
        data = ListingValidation.addImageFieldsToData(
            data,
            [
                $scope.imageUrl,
                $scope.imageUrl2,
                $scope.imageUrl3
            ]
        );

        // Create Listing
        Listing.createListing(data).then(function(resp) {
            // Success
            $scope.isError = false;
            $scope.errorMsg = '';
            if ('data' in resp && 'listing_id' in resp.data) {
                $location.path('/host/' + resp.data.listing_id);
            } else {
                $location.path('/host');
            }
            $window.scrollTo(0, 0);
        }, function(data) {
            // Error
            $scope.isError = true;
            var errorIndex = data.data.error_message.indexOf('Unique index or primary key violation') > -1;
            if ('data' in data && 'error_message' in data.data && errorIndex){
                $scope.errorMsg = 'This user/email already has a listing! Currently ' +
                    'we only support 1 listing per user.';
            } else {
                $scope.errorMsg = 'There was an error while saving. Please try again.';
            }
        });
    };
});
