/**
 * About Controller
 * Endpoint: /about
 *
 * @param LocalStorage
 * @param QueryService
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param Location
 */
app.controller('AboutController', function (
    LocalStorage,
    QueryService,
    $scope,
    $routeParams,
    $location,
    Location
)
{

});
