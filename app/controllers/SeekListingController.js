/**
 * Seek Listing controller
 * Endpoint: /seek
 *
 * @param LocalStorage
 * @param QueryService
 * @param $scope
 * @param $location
 * @param $window
 * @param Location
 * @param Listing
 */
app.controller('SeekListingController', function(
	LocalStorage,
	QueryService,
	$scope,
	$location,
	$window,
	Location,
	Listing
)
{
	var self = this;
	var locations, locationName;
	$scope.locationNames = 'Fetching...';
	$scope.locationIds = [1];
	$scope.selectAllLocations = false;

	Location.getLocations(QueryService).then(function(l) {
		$scope.locations = Location.modifyLocations(l.data);
		for (var i in $scope.locations) {
			// Default Emeryville
			if (i == 0) {
				$scope.locations[i].selected = true;
			} else {
				$scope.locations[i].selected = false;
			}
		}

		var locationNames = [];
		Listing.getListings().then(function(data) {
			$scope.listings = data.data;
			var images, imgUrl;
			for (var i in $scope.listings) {
				$scope.listings[i] = Listing.modifyValues($scope.locations, $scope.listings[i]);
				if (locationNames.indexOf($scope.listings[i].location) === -1) {
					locationNames.push($scope.listings[i].location);
				}
			}
			if (locationNames.length > 0) {
				$scope.locationNames = locationNames.join(', ');
			} else {
				$scope.locationNames = 'N/A';
			}
		}, function() {
			$scope.locationNames = 'N/A';
			$scope.listings = [];
		});
	});

	function selectAllLocations() {
		for (var i in $scope.locations) {
			if ($scope.locationIds.indexOf($scope.locations[i]) === -1) {
				$scope.locations[i].selected = true;
				$scope.locationIds.push($scope.locations[i].location_id)
			}
		}
	}

	$scope.filterClick = function(locationId) {
		if (locationId === 'all') {
			if ($scope.selectAllLocations) {
				$scope.selectAllLocations = false;
				$scope.locationIds = [];
				for (var i in $scope.locations) {
					$scope.locations[i].selected = false;
				}
			} else {
				$scope.selectAllLocations = true;
				selectAllLocations();
			}
		} else {
			var index = $scope.locationIds.indexOf(locationId);
			if (index > -1) {
				$scope.locationIds.splice(index, 1);
			} else {
				$scope.locationIds.push(locationId);
			}
		}
	};
	$scope.filter = function(listing) {
		return $scope.locationIds.indexOf(listing.location_id) > -1;
	};

	$scope.goToSeekDetail = function(seekId) {
		$location.path('/seek/' + seekId);
		$window.scrollTo(0, 0);
	};

	$scope.getDefaultListingImage = function(locationName) {
		var location = Location.getLocation($scope.locations, locationName);
		return location.image_link;
	};
});
