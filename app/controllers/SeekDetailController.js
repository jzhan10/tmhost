/**
 * Seek Detail controller
 * Endpoint: /seek/:id
 *
 * @param LocalStorage
 * @param QueryService
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param Location
 * @param Listing
 * @param Email
 */
app.controller('SeekDetailController', function (
    LocalStorage,
    QueryService,
    $scope,
    $routeParams,
    $location,
    Location,
    Listing,
    Email
)
{
    var self = this;
    var listingId = $routeParams.id;
    var locationName;
    $scope.locations = [];
    $scope.listing = Listing.getLoadingListing();
    $scope.email =
        $scope.locationInfo = '';
    $scope.reserveText = 'Reserve';
    $scope.isReserving =
        $scope.useDefaultLocation = false;

    Location.getLocations(QueryService).then(function(l) {
        $scope.locations = Location.modifyLocations(l.data);

        Listing.getListings().then(function(data) {
            var listing = Listing.getListing(data, listingId);
            if ('name' in listing) {
                $scope.listing = Listing.modifyValues($scope.locations, listing);
                $scope.useDefaultLocation = Listing.modifyImageLink($scope.listing, $scope.locations);
                $scope.locationInfo = Location.getInfoPage($scope.listing.location);
            } else {
                $scope.listing = Listing.getDefaultListing();
            }
        }, function() {
            $scope.listing = Listing.getDefaultListing();
            $scope.title = 'N/A';
        });
    });

    $scope.goToSeekListings = function() {
        $location.path('/seek');
    };

    $scope.goToHostDetail = function() {
        $location.path('/host/' + listingId);
    };

    $scope.reserve = function() {
        $scope.isSuccess = false;
        $scope.successMsg = '';
        $scope.isError = false;
        $scope.errorMsg = '';

        if ($scope.email.length > 0 && !$scope.isReserving) {
            $scope.isReserving = true;
            $scope.reserveText = 'Reserving...';
            $scope.isSuccess =
                $scope.isError = false;
            $scope.successMsg =
                $scope.errorMsg = '';

            var data = {
                listing_id: listingId,
                seeker_email: $scope.email + '@tubemogul.com',
                description: $scope.description || '(No message given)'
            };
            Email.createEmail(data).then(function() {
                $scope.email = '';
                $scope.description = '';
                $scope.isSuccess = true;
                $scope.successMsg = 'Successfully made a request!';
                $scope.isReserving = false;
                $scope.reserveText = 'Reserve';
            });
        } else {
            $scope.isError = true;
            $scope.errorMsg = 'You must enter a valid tubemogul email!';
        }
    };
});
