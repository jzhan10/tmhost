/**
 * Host Listing controller
 * Endpoint: /host
 *
 * @param LocalStorage
 * @param QueryService
 * @param $scope
 * @param $location
 * @param $cookieStore
 * @param Location
 * @param Listing
 */
app.controller('HostListingController', function(
    LocalStorage,
    QueryService,
    $scope,
    $location,
    $cookieStore,
    Location,
    Listing
)
{
    var self = this;
    var locations, locationName;
    $scope.title = 'N/A';
    $scope.showNoGrid = true;
    $scope.gettingListing = false;
    $scope.gettingListingMsg = '';
    Location.getLocations(QueryService).then(function(l) {
        locations = l.data;
    });
    try {
        var recentlyViewedEmail = $cookieStore.get('recentHostEmail');
        if (typeof recentlyViewedEmail !== 'undefined') {
            $scope.recentlyViewedEmail = recentlyViewedEmail + '@tubemogul.com';
        } else {
            $scope.recentlyViewedEmail = 'N/A';
        }
    } catch(e) {
        $cookieStore.remove('recentHostEmail');
        $scope.recentlyViewedEmail = 'N/A';
    }

    function getListingForUser(email) {
        $scope.title = 'Fetching...';

        Listing.getListingForUser(email).then(function(data) {
            if ('data' in data && typeof data.data.name !== 'undefined') {
                data = data.data;


                var name = data.name || 'N/A';
                $scope.title = name + ' - ' + data.email;
                $scope.listing = Listing.modifyValues(locations, data);
                $scope.showNoGrid = false;
            } else {
                $scope.title = 'N/A';
                $scope.listing = false;
                $scope.showNoGrid = true;
            }
            $scope.gettingListing = false;
            $scope.gettingListingMsg = '';
        }, function() {
            $scope.listing = false;
            $scope.title = 'N/A';
            $scope.showNoGrid = true;
            $scope.gettingListing = false;
            $scope.gettingListingMsg = '';
        });
    }

    $scope.getListing = function() {
        $scope.gettingListing = true;
        $scope.gettingListingMsg = 'Fetching listing...';
        $scope.listing = false;
        $cookieStore.put('recentHostEmail', $scope.email );
        $scope.recentlyViewedEmail = $scope.email + '@tubemogul.com';

        getListingForUser($scope.recentlyViewedEmail);
    };

    $scope.goToHostDetail = function(hostId) {
        $location.path('/host/' + hostId);
    };

    $scope.useRecentlySearchedEmail = function() {
        var email = $cookieStore.get('recentHostEmail');
        if (typeof email !== 'undefined') {
            $scope.email = email;
            $scope.getListing();
        }
    };
});
