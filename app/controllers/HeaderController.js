/**
 * Header Controller - Determine highlighted/current page
 *
 * @param $scope
 */
app.controller('HeaderController', function($scope) {
	function checkOtherPaths() {
        if (location.hash.indexOf('#/host/') > -1) {
            $scope.menuSelected = 'host';
        } else if (location.hash.indexOf('#/seek/') > -1) {
            $scope.menuSelected = 'seek';
        } else {
			$scope.menuSelected = '';
		}
	}
    function setSelected() {
        var path = location.hash;
        switch (path) {
            case '#/about':
                $scope.menuSelected = 'about';
                break;
            case '#/host/create':
                $scope.menuSelected = 'host_create';
                break;
            case '#/host':
                $scope.menuSelected = 'host';
                break;
            case '#/seek':
                $scope.menuSelected = 'seek';
                break;
            default:
				checkOtherPaths();
        }
    }
    setSelected();
    $scope.$on('$routeChangeSuccess', function(scope, next, current){
        setSelected();
    });
});
