/**
 *
 * AngularJS Boilerplate
 * @description           Description
 * @author                Jozef Butko // www.jozefbutko.com/resume
 * @url                   www.jozefbutko.com
 * @version               1.1.7
 * @date                  March 2015
 * @license               MIT
 *
 */
/**
* Definition of the main app module and its dependencies
*/
var app = angular.module('boilerplate', ['ngRoute', 'ngCookies']);
var host = 'http://52.24.176.105:8083';

;(function() {
    // Jeff: Do we even need this?
    // safe dependency injection
    // this prevents minification issues
    // config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider'];

    /**
     * App routing
     *
     * You can leave it here in the config section or take it out
     * into separate file
     *
     */
    app.config(function(
        $routeProvider,
        $locationProvider,
        $httpProvider,
        $compileProvider
    )
    {
        $locationProvider.html5Mode(false);
        // routes
        $routeProvider
            .when('/', {
                templateUrl: 'views/home.html',
                controller: 'MainController',
                controllerAs: 'main'
            })
            .when('/contact', {
                templateUrl: 'views/contact.html',
                controller: 'MainController',
                controllerAs: 'main'
            })
            .when('/setup', {
                templateUrl: 'views/setup.html',
                controller: 'MainController',
                controllerAs: 'main'
            })
            .when('/host/create', {
                templateUrl: 'views/apply_host.html',
                controller: 'HostApplyController',
                controllerAs: 'hostApply'
            })
            .when('/host', {
                templateUrl: 'views/host_listing.html',
                controller: 'HostListingController',
                controllerAs: 'hostListing'
            })
            .when('/host/:id', {
                templateUrl: 'views/host_detail.html',
                controller: 'HostDetailController',
                controllerAs: 'hostDetail'
            })
            .when('/seek', {
                templateUrl: 'views/seek_listing.html',
                controller: 'SeekListingController',
                controllerAs: 'seekListing'
            })
            .when('/seek/:id', {
                templateUrl: 'views/seek_detail.html',
                controller: 'SeekDetailController',
                controllerAs: 'seekDetail'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutController',
                controllerAs: 'about'
            })
            .otherwise({
                redirectTo: '/'
            });
        $httpProvider.interceptors.push('authInterceptor');
    });

    /**
     * You can intercept any request or response inside authInterceptor
     * or handle what should happend on 40x, 50x errors
     *
     */
    app.factory('authInterceptor', function($rootScope, $q, LocalStorage, $location) {
        return {
            // intercept every request
            request: function(config) {
                config.headers = config.headers || {};
                return config;
            },
            // Catch 404 errors
            responseError: function(response) {
                if (response.status === 404) {
                    $location.path('/');
                    return $q.reject(response);
                } else {
                    return $q.reject(response);
                }
            }
        };
    });

    /**
     * Run block
     */
    app.run(function($rootScope, $location) {
        // put here everything that you need to run on page load
    });
})();
