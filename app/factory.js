/**
 * Shared functions between services
 *
 */
/**
* Format JSON to String Params
*/
function formatData(data) {
    return Object.keys(data).map(function(k) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
    }).join('&')
}
