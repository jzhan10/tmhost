app.directive('fallbackImage', function() {
	var fallbackSrc = {
		link: function postLink(scope, element, attrs) {
			var defaultImage = '/images/default-placeholder.png';
			defaultImage = attrs.fallbackSrc;
			element.bind('error', function() {
				angular.element(this).attr("src", defaultImage);
			});
		}
	}
	return fallbackSrc;
});
