;(function() {

  'use strict';

  /**
   * Footer, just a HTML template
   * @author Jozef Butko
   * @ngdoc  Directive
   *
   * @example
   * <footer></footer>
   *
   */
  angular
    .module('boilerplate')
    .directive('footer', tinFooter);

  function tinFooter() {

    // Definition of directive
    var directiveDefinitionObject = {
      restrict: 'E',
      templateUrl: 'components/directives/footer.html'
    };

    return directiveDefinitionObject;
  }

})();
